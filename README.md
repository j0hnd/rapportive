# This is my README
# notes:
#
# 1. fake email address generated  through a random strings can be used in
#    acquiring session token until limit per day is not reached
#
# 2. 40 request per email within a day
#
# 3. after reaching limit, "too many connections" will occur
#
# 4. valid session token can't be self automate. due to,
#
#    - we need to know how this tokens are created, like the
#      algorithm used
#
#    - the composition of its readable string before it is hashed
