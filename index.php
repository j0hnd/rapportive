<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
<form id="rapportive-form" method="post" class="left mr12">
    <fieldset>
        <legend>email address</legend>
        <label>first name</label>
        <input type="text" id="id-firstname" name="firstname" />

        <label>last name</label>
        <input type="text" id="id-lastname" name="lastname" />

        <label>domain</label>
        <input type="text" id="id-domain" name="domain" value="gmail.com" />

        <input type="checkbox" id="use_proxy_id" name="use_proxy" value="1" class="use-proxy left" checked />
        <label class="left" for="use_proxy_id" style="width: 70px;">Use Proxy</label>

        <br class="clear" />
    </fieldset>

    <input type="button" class="toggle-search" value="Search" />
</form>

<form id="rapportive-import" action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/rapportive/rapportive.php?action=import" method="post" enctype="multipart/form-data" class="left ml12" style="margin-top:-11px;">
    <fieldset class="mt12">
        <legend>import</legend>
        <input type="file" id="import" name="import" />
        <label class="mt12">send results to</label>
        <input type="text" id="id-sendto" name="sendto" value="aevans@friendseat.com" />
        <div class="progress"></div>
        <input type="checkbox" id="use_proxy_id" name="use_proxy" value="1" class="use-proxy left" checked />
        <label class="left" for="use_proxy_id" style="width: 70px;">Use Proxy</label>
    </fieldset>

    <input type="submit" class="toggle-import" value="Import" />
</form>
<div class="clear"></div>
<div class="email-results left">
    <h4>Email Results <label class="result-count"></label></h4>
    <div class="loader-email hide"><span></span></div>
    <div class="results-container"></div>
</div>
<div class="rapportive-results-container left">
    <h4>Rapportive Results</h4>
    <div class="loader-rapportive hide"><span></span></div>
    <div class="rapportive-results"></div>
</div>

<style type="text/css">


</style>
<script type="text/javascript">
    $(function(){
        var requestUrl = 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/rapportive/rapportive.php';
        var emails = [];
        var qs = getQueryStrings();

        function progressHandlingFunction(e){
            if(e.lengthComputable){
                $('.progress').attr({value:e.loaded,max:e.total});
            }
        }

        $( '.toggle-search' ).on( 'click', function(){
            $.ajax({
                url: requestUrl + '/?action=get_email_address',
                method: 'post',
                data: {
                    data: $('#rapportive-form').serialize()
                },
                dataType: 'json',
                beforeSend: function () {
                    if ( $('#id-firstname').val().length == 0 || $('#id-lastname').val().length == 0 || $('#id-domain').val().length == 0 ) {
                        alert( 'Error! Empty form' );
                        return false;
                    }
                    else {
                        $( '.loader-email' ).removeClass( 'hide' );
                    }
                },
                success: function ( resp ){
                    $( '.loader-email' ).addClass( 'hide' );
                    emails = resp.data;

                    if ( resp.status == 'OK' ) {
                        var row = '';
                        $.each( resp.data, function( k, v ){
                            row += '<span>' + v + '</span>';
                        });

                        $( '.results-container' ).html( row );

                        // check if flag to use reserved logins is set
                        if ( qs['reserved'] != undefined ) {
                            requestUrl = requestUrl + '/?reserved=' + qs['reserved'];
                        }

                        var use_proxy = $('#use_proxy_id').get(0).checked;

                        // trigger rapportive queries
                        rapportiveRequest( use_proxy, emails );
                    }
                }
            });
        });

        function rapportiveRequest( use_proxy, emails ) {
            $.ajax({
                url: requestUrl + '/?action=rapportive_request',
                method: 'post',
                data: {
                    use_proxy: use_proxy, data: emails
                },
                dataType: 'json',
                beforeSend: function () {
                    $( '.loader-rapportive' ).removeClass( 'hide' );
                },
                error: function ( resp ) {
                    $( '.loader-rapportive' ).addClass( 'hide' );
                    $( '.rapportive-results' ).html( resp.responseText );
                },
                success: function ( resp ) {
                    $( '.loader-rapportive' ).addClass( 'hide' );
                    var content = '';

                    if ( resp.status == 'OK' ) {
                        $.each( resp.data, function( email, data ){
                            content += '<h3>' + email + '</h3>';
                            $.each( data, function( k, v ){
                                if ( k == 'headline' || k == 'email' || k == 'first_name' || k == 'last_name' || k == 'locations' ) {
                                    content += '<span class="row-label">'+ k.replace('_', ' ') +':</span><span class="row-value">' + v + '</span>';
                                }
                                if ( k == 'occupations' ) {
                                    content += '<span class="row-label">Occupations:</span>';
                                    content += '<div class="sub-details clear">';
                                    $.each( v, function( _k, _v ){
                                        $.each( _v, function( __k, __v ){
                                            if ( __k == 'company' || __k == 'job_title' ) {
                                                content += '<span class="row-label">'+ __k.replace('_', ' ') +':</span><span class="row-value">' + __v + '</span>';
                                            }
                                        });
                                    });
                                    content += '</div>';
                                }
                                if ( k == 'memberships' ) {
                                    content += '<span class="row-label">Memberships:</span>';
                                    content += '<div class="sub-details clear">';
                                    $.each( v, function( _k, _v ){
                                        $.each( _v, function( __k, __v ){
                                            if ( __k == 'display_name' || __k == 'username' || __k == 'profile_id' ) {
                                                content += '<span class="row-label">'+ __k.replace('_', ' ') +':</span><span class="row-value">' + __v + '</span>';
                                            }

                                            if ( __k == 'profile_url' ) {
                                                content += '<span class="row-label">'+ __k.replace('_', ' ') +':</span><span class="row-value"><a href="' + __v + '">' + __v + '</a></span>';
                                            }
                                        });
                                    });
                                    content += '</div>';
                                }
                            });
                        });

                        $('.rapportive-results').append( content );
                        $('.navlist').show();
                    }
                    else {
                        $('.rapportive-results').html( '<p style="color: red; font-weight: bold;">' + resp.status + '! ' + resp.message + '</p>');
                    }
                }
            });
        }

        function getQueryStrings() {
            var assoc  = {};
            var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
            var queryString = location.search.substring(1);
            var keyValues = queryString.split('&');

            for(var i in keyValues) {
                var key = keyValues[i].split('=');
                if (key.length > 1) {
                    assoc[decode(key[0])] = decode(key[1]);
                }
            }

            return assoc;
        }
    });
</script>
