<?php
define( 'ABSPATH', dirname( __FILE__ ) );
require_once 'lib/rapportive.class.php';

$action     = $_REQUEST['action'];
$data       = isset( $_REQUEST['data'] ) ? $_REQUEST['data'] : null;
$use_proxy  = isset( $_REQUEST['use_proxy'] ) ? $_REQUEST['use_proxy'] : null;
$send_to    = isset( $_REQUEST['sendto'] ) ? $_REQUEST['sendto'] : null;
$write      = isset( $_REQUEST['write'] ) ? $_REQUEST['write'] : null;
$rapportive = new Rapportive();
switch ( $action ) {
    case 'get_email_address':
        $request_arr = array();
        $request_arr = $rapportive->parseRequestData( $data );

        if ( is_array( $request_arr ) ) {
            $rapportive->setDomainName( $request_arr['domain'] );
            $output = $rapportive->generateEmailAddress( strtolower(trim($request_arr['firstname'])) . ' ' . strtolower(trim(($request_arr['lastname']))) );

            if ( isset( $request_arr['token'] ) ) {
                $rapportive->setSessionToken( $request_arr['token'] );
            }

            if ( count( $output ) > 0 ) {
                $response = array(
                    'status'  => 'OK',
                    'message' => 'Success',
                    'data'    => $output
                );
            }
            else {
                $response = array(
                    'status'  => 'FAIL',
                    'message' => 'Email address not generated'
                );
            }

            break;
        }

        $response = array(
            'status'  => 'FAIL',
            'message' => 'Invalid request'
        );

        break;

    case 'import':
        if ( $_FILES['import']['size'] != 0 && $send_to != '' ) {
            $emails = $rapportive->doImport( $_FILES['import']['tmp_name'] );
            $request = $rapportive->doRequest( $emails, $use_proxy );

            if ( $request['status'] == 'OK' ) {
                $result = $rapportive->doExport( $request['data'], $send_to );
                if ( $result == 'OK' ) {
?>
                    <style>
                        .link {
                            font-size: 12px;
                            text-decoration: none;
                            display: block;
                            margin: -18px 0 10px 0;
                        }
                    </style>
                    <table>
                        <tr>
                            <td colspan="2">
                                <h3>Results:</h3>
                                <a href="http://<?php echo $_SERVER['HTTP_HOST']?>/rapportive" class="link">Back</a>
                            </td>
                        </tr>
<?php
                    foreach ( array_slice( $rapportive->getData(), 1 ) as $data ) {
                        foreach ( $data as $k => $d ) {
?>
                            <tr>
                                <td style="width: 172px; background-color: #E5E5E5"><?php echo ucfirst( str_replace( '_', ' ', $k) ); ?></td>
                                <td><?php echo $d ?></td>
                            </tr>
<?php
                        }
?>
                            <tr>
                                <td colspan="2" style="height: 25px;">&nbsp;</td>
                            </tr>
                    <?php
                    }
?>
                    </table>
<?php
                    exit;
                }
            }
            else {
                header( 'Location: http://'.$_SERVER['HTTP_HOST'].'/rapportive' );
                ob_clean();
                flush();
            }
        }
        break;

    case 'cleanlog':
        $rapportive->doCleanLog();
        header( 'Location: http://'.$_SERVER['HTTP_HOST'].'/rapportive' );
        ob_clean();
        flush();
        break;

    case 'rapportive_request':
        if ( is_array( $data ) ) {
            $result = array();
            $check = false;

            $result = $rapportive->doRequest( array( $data ), $use_proxy );
            $response = array(
                'status'  => $result['status'],
                'message' => '',
                'data'    => $result['data']
            );
        }

        break;

    /*
     * Test calls
     *
     */
    case 'testmail':
        if ( $rapportive->sendMail( $send_to, 'Test Mail' ) ) {
            $response = array(
                'status'  => 'OK',
                'message' => 'mail sent'
            );
        }
        else {
            $response = array(
                'status'  => 'FAIL',
                'message' => 'mail not sent'
            );
        }

        break;

    case 'testsignin':
        if ( $rapportive->rapportiveSignIn( true ) ) {
            $status = 'OK';
            $data = array(
                'token' => $rapportive->getSessionToken(),
                'proxy' => $rapportive->getProxyAddress()
            );
        }
        else {
            $status = 'FAIL';
            $data = null;
        }

        $response = array(
            'status'  => $status,
            'message' => 'requesting token',
            'data'    => $data
        );
        break;

    case 'testproxy':
        $proxy = $rapportive->getProxyInfo();
        if ( (int) $write ) {
            $rapportive->log( $rapportive->getKey() );
        }

        $response = array(
            'status'  => 'OK',
            'message' => 'requesting proxy details',
            'key'     => $rapportive->getKey(),
            'data'    => $proxy
        );
        break;

    default:
        $response = array(
            'status'  => 'FAIL',
            'message' => 'Invalid request'
        );

        break;
}

echo json_encode( $response );
exit;
