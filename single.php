<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
<form id="session-token-form" method="post" class="left">
    <fieldset>
        <legend>Get Session Token</legend>
        <label>Email ID</label>
        <input type="text" name="email_address" />

        <label>Use Proxy Address</label>
        <input type="text" name="proxy_address" />
    </fieldset>

    <input type="button" class="toggle-get" value="Get" />
</form>
<div class="session-token-container left mt20 ml30 mb20">
    <label>Session Token</label>
    <span class="session-token-holder"></span>
    <div class="loader-session hide"><span></span></div>
</div>

<div class="clear"></div>

<div class="session-token-input-container hide ml12">
    <label>Enter session token manually:</label>
    <textarea class="session-token-input"></textarea>
</div>

<div class="hide rapportive-container">
    <form id="rapportive-form" method="post">
        <fieldset>
            <legend>email address</legend>
            <label>first name</label>
            <input type="text" name="firstname" />

            <label>last name</label>
            <input type="text" name="lastname" />

            <label>domain</label>
            <input type="text" name="domain" value="gmail.com" />
        </fieldset>

        <input type="button" class="toggle-search" value="Search" />
    </form>
    <div class="email-results left">
        <h4>Email Results <label class="result-count">(results: <label class="count"></label>)</label></h4>
        <div class="loader-email hide"><span></span></div>
        <div class="results-container"></div>
    </div>
    <div class="rapportive-results-container left">
        <h4>Rapportive Results</h4>
        <div class="loader-rapportive hide"><span></span></div>
        <div class="rapportive-results"></div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var requestUrl = 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/rapportive/rapportive.php';

        $( '.toggle-get' ).on( 'click', function ( e ) {
            e.preventDefault();
            $.ajax({
                url: requestUrl + '?action=get_session_token',
                dataType: 'json',
                method: 'post',
                data: { data: $( '#session-token-form' ).serialize() },
                beforeSend: function () {
                    $( '.loader-session' ).removeClass( 'hide' );
                },
                success: function ( resp ) {
                    $( '.loader-session' ).addClass( 'hide' );
                    var content = ( resp.data ) ? resp.data : resp.message;
                    $( '.session-token-holder' ).html( content );
                    $( '.rapportive-container' ).show();
                    if ( resp.status == 'FAIL' ) {
                        $( '.session-token-input-container' ).show();
                    }
                }
            });
        });

        $( '.toggle-search' ).on( 'click', function ( e ) {
            var emails;
            $.ajax({
                url: requestUrl + '?action=get_email_address',
                method: 'post',
                data: {
                    data: $('#rapportive-form').serialize() + '&token=' + $( '.session-token-input' ).val()
                },
                dataType: 'json',
                beforeSend: function () {
                    $( '.loader-email' ).removeClass( 'hide' );
                },
                success: function ( resp ) {
                    $( '.loader-email' ).addClass( 'hide' );
                    emails = resp.data;

                    if ( resp.status == 'OK' ) {
                        var row = '';
                        $( '.count' ).text( resp.data.length );
                        $.each( resp.data, function( k, v ){
                            row += '<span>' + v + '</span>';
                        });

                        $( '.results-container' ).html( row );
                    }

                    // execute rapportive queries
                    $.ajax({
                        url: requestUrl + '?action=rapportive_request',
                        method: 'post',
                        data: {
                            data: emails
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $( '.loader-rapportive' ).removeClass( 'hide' );
                        },
                        error: function ( resp ) {
                            $( '.loader-rapportive' ).addClass( 'hide' );
                            $( '.rapportive-results' ).html( resp.responseText );
                        },
                        success: function ( resp ) {
                            $( '.loader-rapportive' ).addClass( 'hide' );
                            if ( resp.status == 'OK' ) {
                                var row = '';
                                if ( resp.data ) {
                                    $.each( resp.data, function( k1, v1 ){
                                        if ( v1 ) {
                                            $.each( v1.contact, function( k2, v2 ){
                                                row += '<pre>' + k2 + ': ' + v2 + '</pre>';
                                            });
                                            row += '--------------------------------------------------<br/>';
                                        }
                                        else {
                                            row += 'Oops! Query returned a null result<br/>';
                                        }
                                    });
                                }
                            }
                            else {
                                row = '<pre>' + resp.message + '</pre>';

                            }

                            $( '.rapportive-results' ).html( row );
                        }
                    });
                }
            });
        });
    });
</script>