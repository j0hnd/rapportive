<?php
require_once 'rapportiveBase.class.php';

ob_start();
session_start();

class Rapportive extends rapportiveBase
{
    private $domain = '';
    private $email_address = '';
    private $send_to = '';
    private $names = array();
    private $pattern = array('{fn}','{fn}{ln}','{fl}{ln}','{fn}.{ln}','{fn}_{ln}','{fn}-{ln}','{fn}{ll}','{fl}{ll}');

    public $test_mode = false;


    public function rapportiveSignIn( $use_proxy = false )
    {
        $session_token = $this->getSessionToken();
        $success = true;

        if ( empty( $session_token ) || $session_token == '' ) {
            $url = $this->generateLoginStatusURL();
            if ( $url ) {
                $output = $this->doCurl( $url, $use_proxy );
                $result = json_decode( $output, true );

                if ( is_null( $output ) ) {
                    $success = false;
                }
                else {
                    if ( array_key_exists( 'error', $result ) !== false || array_key_exists( 'error_code', $result ) !== false ) {
                        $success = false;
                        $this->log( $this->getKey() );
                    }
                    else {
                        $this->setSessionToken( $result['session_token'] );
                    }
                }
            }
            else {
                $success = false;
            }
        }

        return $success;
    }

    public function generateEmailAddress( $string )
    {
        $string_arr = explode( " ", $string );
        $results = array();
        $names = array();

        // reset
        $this->setSessionToken('');
        $this->setProxyAddress('');
        $this->setData('');

        if ( count($string_arr) > 0 ) {

            array_push( $names, array( $string_arr[0], $string_arr[1], $this->getDomainName() ) );
            $this->setNames( $names );

            foreach ( $this->pattern as $key => $pattern ) {
                // firstname | Domain
                if ( $key == 0 ) $email = str_replace($pattern, $string_arr[0].$this->getDomainName(), $pattern );

                // firstnamelastname | domain
                if ( $key == 1 ) $email = str_replace($pattern, $string_arr[0].$string_arr[1].$this->getDomainName(), $pattern );

                // firstletterlastname | domain
                if ( $key == 2 ) $email = str_replace($pattern, substr( $string_arr[0], 0, 1 ).$string_arr[1].$this->getDomainName(), $pattern );

                // firstname.lastname | domain
                if ( $key == 3 ) $email = str_replace($pattern, $string_arr[0].'.'.$string_arr[1].$this->getDomainName(), $pattern );

                // firstname_lastname | domain
                if ( $key == 4 ) $email = str_replace($pattern, $string_arr[0].'_'.$string_arr[1].$this->getDomainName(), $pattern );

                // firstname-lastname | domain
                if ( $key == 5 ) $email = str_replace($pattern, $string_arr[0].'-'.$string_arr[1].$this->getDomainName(), $pattern );

                // firstnamelastletter| domain
                if ( $key == 6 ) $email = str_replace($pattern, $string_arr[0].substr( $string_arr[1], 0, 1 ).$this->getDomainName(), $pattern );

                // firstletterlastletter| domain
                if ( $key == 7 ) $email = str_replace($pattern, substr( $string_arr[0], 0, 1 ).substr( $string_arr[1], 0, 1 ).$this->getDomainName(), $pattern );

                array_push( $results, $email );
            }
        }

        return $results;

    }

    public function doRapportive( $email, $use_proxy = false )
    {
        $data = null;

        if ( $this->getSessionToken() == ''  ) {
            $result = $this->rapportiveSignIn( $use_proxy );
        }

        $url = "https://profiles.rapportive.com/contacts/email/$email";
        $json_result = $this->doCurl( $url, $use_proxy, true, $this->getSessionToken() );
        $data = json_decode( $json_result, true );


        return $data;
    }

    public function doImport( $file )
    {
        $emails = array();

        $data = $this->import($file);
        if ( count( $data ) > 0 ) {
            foreach ( $data as $row ) {
                $str = explode( " ", $row );
                $this->setDomainName( $str[2] );
                $emails[] = $this->generateEmailAddress( $str[0] . ' ' . $str[1] );
            }
        }

        return $emails;
    }

    public function doExport( $data, $to )
    {
        $status = 'FAIL';
        $result = $this->export( $data, $to );

        if ( $result === true ) {
            $status = 'OK';
        }

        return $status;
    }

    public function doRequest( $emails, $use_proxy = false )
    {
        $result = $_data = array();
        $check = false;
        $notfound = 0;
        $this->setSessionToken(''); // force reset

        foreach ( $emails as $email ) {
            foreach ( $email as $e ) {
                $query = $this->doRapportive( $e, $use_proxy );
                if ( isset( $query['contact']['memberships'] ) && count( $query['contact']['memberships'] ) > 0 ) {
                    array_push( $result, array( $e => $query['contact'] ) );
                    $check = true;
                }
                else {
                    $notfound++;
                }

                unset( $query );
            }
        }

        if ( count( $result ) > 0 ) {
            if ( array_key_exists( 'error_code', $result ) !== false || array_key_exists( 'error', $result ) !== false ) {
                $status = 'FAIL';
            }
            else {
                $status = 'OK';
                if ( $check === true ) {
                    foreach ( $result as $data ) {
                        foreach ( $data as $k => $d) {
                            $email = explode( "@", $k );
                            $_data[$k]['first_name'] = $d['first_name'];
                            $_data[$k]['last_name'] = $d['last_name'];
                            $_data[$k]['domain'] = $email[1];
                            $_data[$k]['email'] = $k;
                            $_data[$k]['location'] = $d['location'];
                            $_data[$k]['occupations'] = $d['occupations'];
                            $_data[$k]['organisation'] = $d['organisation'];
                            $_data[$k]['memberships'] = $d['memberships'];
                        }
                    }
                }
            }
        }
        else {
            if ( $notfound > 0 ) {
                $status = '***No results found';
            }
            else {
                $status = 'FAIL';
            }
        }

        return array(
            'status' => $status,
            'data'   => $_data
        );
    }

    public function doCleanLog()
    {
        file_put_contents( $this->getLogFile(), "" );
    }

    /***************************************************************************************/
    /**** private methods                                                               ****/
    /***************************************************************************************/

    private function doCurl( $url, $use_proxy, $headers = false, $session_token = '' )
    {
        $curl = curl_init();
        $proxy = null;
        $proxy_str = '';

        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_URL, $url );

        $use_proxy = filter_var($use_proxy, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

        if ( $use_proxy === true ) {
            $proxy = $this->getProxyInfo();
            if ( $this->getProxyAddress() ) {
                $proxy_address = $this->getProxyAddress();
            }
            else {
                $proxy_address = $proxy['ip'];
                $this->setProxyAddress( $proxy_address );
            }

            $proxy_str = "{$proxy['user']}:{$proxy['pass']}@{$proxy_address}:{$proxy['port']}";
            curl_setopt( $curl, CURLOPT_PROXY, $proxy_str );
        }

        if ( $headers ) {
            curl_setopt( $curl, CURLOPT_HTTPHEADER, array( 'X-Session-Token: '.$session_token ) );
        }

        $result = curl_exec( $curl );

        curl_close( $curl );

        return $result;
    }

    private function generateLoginStatusURL()
    {
        return "https://rapportive.com/login_status?user_email=".$this->randomString()."@gmail.com";
    }


    /***************************************************************************************/
    /**** getters and setters                                                           ****/
    /***************************************************************************************/
    public function setDomainName( $value )
    {
        $this->domain = '@'.$value;
    }

    private function getDomainName()
    {
        return $this->domain;
    }

    public function setSessionToken( $value )
    {
        $_SESSION['session_token'] = $value;
    }

    public function getSessionToken()
    {
        return $_SESSION['session_token'];
    }

    public function setEmailAddress( $value )
    {
        $this->email_address = $value;
    }

    public function getEmailAddress()
    {
        return $this->email_address;
    }

    public static function setProxyAddress( $value )
    {
        if ( $value != '' || !is_null( $value ) ) $_SESSION['proxy_address'] = $value;
    }

    public static function getProxyAddress()
    {
        return $_SESSION['proxy_address'];
    }

    public function setSendTo( $value )
    {
        $this->send_to = $value;
    }

    public function getSendTo()
    {
        return $this->send_to;
    }

    public function getProxyInfo()
    {
        return parent::getProxyInfo();
    }

    public function setKey( $value )
    {
        parent::setKey( $value );
    }

    public function getKey()
    {
        return parent::getKey();
    }

    public function setNames( $value )
    {
        parent::setNames( $value );
    }

    public function getNames()
    {
        parent::getNames();
    }

    public function setData( $data )
    {
        parent::setData( $data );
    }

    public function getData()
    {
        return parent::getData();
    }
}