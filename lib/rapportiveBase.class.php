<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once 'swift/lib/swift_required.php';

abstract class rapportiveBase
{
    protected static $key = null;

    public static function import( $file )
    {
        $data = array();
        $csv = file_get_contents( $file );
        foreach ( explode( "\r\n", $csv ) as $row ) {
            $data[] = str_replace( ",", " ", $row );
        }

        return $data;
    }

    public static function export( $data, $to )
    {
        $data = self::populateData( $data );
        self::setData( $data );
        self::createAttachment( $data );
        return self::sendMail( $to, 'Rapportive Queries' );
    }

    public static function parseRequestData( $data )
    {
        $result = array();
        parse_str( $data, $result );

        return $result;
    }

    public static function randomString( $length = 7 )
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ( $i = 0; $i < $length; $i++ ) {
            $randomString .= $characters[ rand( 0, strlen( $characters ) - 1 ) ];
        }

        return $randomString;
    }

    public static function log( $key = '' )
    {
        $log_file = self::getLogFile();
        $csv = file_get_contents( $log_file );
        $data = str_getcsv( $csv, ',' );
        array_push( $data, $key );

        $handle = fopen( $log_file, 'w' );
        fputcsv( $handle, $data );
        fclose( $handle );
    }

    public static function readLog( $key )
    {
        # return null if key is blocked,
        # return key if key is available
        $result = $key;

        $log_file = self::getLogFile();
        $csv = file_get_contents( $log_file );
        $data = str_getcsv( $csv, ',' );
        if ( in_array( $key, $data ) === true ) {
            $result = null;
        }

        return $result;
    }

    public static function sendMail( $recipient, $subject )
    {
        $file = ABSPATH . "/csv/export.csv";
        $smtp = self::getSMTPInfo();

        $transporter = Swift_SmtpTransport::newInstance($smtp['gmail']['host'], $smtp['gmail']['port'], $smtp['gmail']['encryption']);
        $transporter->setUsername($smtp['gmail']['username']);
        $transporter->setPassword($smtp['gmail']['password']);

        $mailer = Swift_Mailer::newInstance($transporter);

        $message = Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom(array('no-reply@socialdraft.com' => 'SocialDraft'));
        $message->setTo($recipient);
        $message->setBody('Rapportive query results');
        $message->attach(Swift_Attachment::fromPath($file));

        return $mailer->send($message);
    }

    public static function debug( $data, $exit = false )
    {
        echo 'DEBUG:<pre>'; print_r( $data ); echo '</pre>';
        if ( $exit ) exit;
    }


    /***************************************************************************************/
    /**** private static methods                                                        ****/
    /***************************************************************************************/

    private static function populateData( $data )
    {
        $_data = $info = $others = array();

        $headers = array(
            'Firstname',
            'Lastname',
            'Domain',
            'Email',
            'Locations',
            'Company',
            'Job Title',
            'Facebook',
            'LinkedIn',
            'Google+',
            'Twitter',
            'Flickr',
            'Others'
        );

        if ( count( $data ) > 0 ) {
            array_push( $_data, $headers );

            foreach ( $data as $k => $value ) {

                $info['first_name'] = '';
                $info['last_name'] = '';
                $info['domain'] = '';
                $info['email'] = '';
                $info['location'] = '';

                $others['company'] = '';
                $others['job_title'] = '';
                $others['facebook'] = '';
                $others['linkedin'] = '';
                $others['google+'] = '';
                $others['twitter'] = '';
                $others['flickr'] = '';
                $others['others'] = '';

                foreach ( $value as $_k => $_v ) {
                    if (( $_k == 'occupations' ) || ( $_k == 'memberships' ) ) {
                        foreach ( $_v as $__k => $__v ) {
                            foreach ( $__v as $___k => $___v ) {
                                if ( $___k == 'company' ) {
                                    $others['company'] = $___v;
                                }
                                elseif ( $___k == 'job_title' ) {
                                    $others['job_title'] = $___v;
                                }
                                elseif ( $___k == 'display_name' ) {

                                    if ( $___v == 'Facebook' ) {
                                        $others['facebook'] = $__v['profile_url'];
                                    }
                                    elseif ( $___v == 'LinkedIn' ) {
                                        $others['linkedin'] = $__v['profile_url'];
                                    }
                                    elseif ( $___v == 'Google+' ) {
                                        $others['google+'] = $__v['profile_url'];
                                    }
                                    elseif ( $___v == 'Twitter' ) {
                                        $others['twitter'] = $__v['profile_url'];
                                    }
                                    elseif ( $___v == 'Flickr' ) {
                                        $others['flickr'] = $__v['profile_url'];
                                    }
                                    else {
                                        $others['others'] = $others['others'].' | '.$__v['profile_url'];
                                    }

                                }
                            }
                        }
                    }
                    elseif ( $_k == 'first_name') {
                        $info['first_name'] = $_v;
                    }
                    elseif ( $_k == 'last_name') {
                        $info['last_name'] = $_v;
                    }
                    elseif ( $_k == 'domain') {
                        $info['domain'] = $_v;
                    }
                    elseif ( $_k == 'email') {
                        $info['email'] = $_v;
                    }
                    elseif ( $_k == 'location') {
                        $info['location'] = $_v;
                    }
                }

                $merge = array_merge( $info, $others );
                array_push( $_data, $merge );

                $info = array();
                $others = array();
                unset( $merge );
            }
        }

        return $_data;
    }

    private static function outputCSV( $data )
    {
        $outputBuffer = fopen( "php://output", 'w' );
        if ( $outputBuffer !== false ) {
            foreach( $data as $val ) {
                fputcsv( $outputBuffer, $val,',', ' ' );
            }
            fclose( $outputBuffer );
        }
    }

    private static function createAttachment( $data )
    {
        $file = ABSPATH . "/csv/export.csv";
        $outputBuffer = fopen( $file, "w" );
        if ( $outputBuffer !== false ) {
            foreach( $data as $val ) {
                fputcsv( $outputBuffer, $val, ',', ' ' );
            }
            fclose( $outputBuffer );
        }
    }


    /***************************************************************************************/
    /**** protected methods                                                             ****/
    /***************************************************************************************/

    protected function getLogFile()
    {
        return ABSPATH . '/rapportive.log';
    }

    protected function getSMTPInfo()
    {
        return array(
          'gmail' => array(
            'host'       => 'smtp.googlemail.com',
            'username'   => 'noreplyrapp@gmail.com',
            'password'   => 'Q1w2e3r4t%',
            'port'       => 465,
            'encryption' => 'ssl'
          )
        );
    }

    protected function getProxyInfo()
    {
        $proxy = array(
            array( 'ip' => '142.91.200.166', 'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.174', 'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.216', 'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.247', 'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.47',  'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.6',   'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.78',  'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '205.164.51.104', 'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '205.164.51.129', 'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' ),
            array( 'ip' => '142.91.200.29',  'port' => '29842', 'user' => 'asmith', 'pass' => 'Gc8j2JfR' )
        );

        $key = array_rand( $proxy, 1 );
        $ref_key = self::readLog( $key );
        if ( is_null( $ref_key ) ) {
            self::getProxyInfo();
        }
        else {
            self::setKey( $ref_key );
        }

        return $proxy[$key];
    }

    protected function setKey( $value )
    {
        self::$key = $value;
    }

    protected function getKey()
    {
        return self::$key;
    }

    protected function setNames( $value )
    {
        if ( $value != '' || !is_null( $value ) ) $_SESSION['names'] = $value;
    }

    protected function getNames()
    {
        return $_SESSION['names'];
    }

    protected function setData( $data )
    {
        if ( is_array( $data ) && count( $data ) > 0 ) $_SESSION['data'] = $data;
    }

    protected function getData()
    {
        return $_SESSION['data'];
    }
}
