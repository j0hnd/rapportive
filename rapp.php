<?php
error_reporting(E_ALL);
require_once 'lib/rapportive.class.php.20140103';

$fname = $_POST['firstname'];
$lname = $_POST['lastname'];
$domain = $_POST['domain'];

if ( !empty( $fname ) && !empty( $lname ) && !empty( $domain ) ) {
    $string = "$fname $lname";

    //$rapportive = new Rapportive();
    Rapportive::singleton()->setDomainName( $domain );
    $output = Rapportive::singleton()->generateEmailAddress( $string );
}
?>

<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
<form method="post">
    <fieldset>
        <legend>email address</legend>
        <label>first name</label>
        <input type="text" name="firstname" />

        <label>last name</label>
        <input type="text" name="lastname" />

        <label>domain</label>
        <input type="text" name="domain" value="gmail.com" />
    </fieldset>

    <input type="submit" value="Search" />
</form>

<?php if ( isset( $output ) ) : ?>
<div class="email-results left">
    <h4>Email Results <label class="result-count">(results: <?php echo count( $output ); ?>)</label></h4>
    <?php foreach ( $output as $out ) : ?>
    <span><?php echo $out; ?></span>
    <?php endforeach; ?>
</div>
<div class="rapportive-results-container left">
    <h4>Rapportive Results</h4>
    <div class="rapportive-results">
   <?php
    foreach ( $output as $out ) {
        $result = Rapportive::singleton()->doRapportive( $out );
        if ( $result ) {
            echo 'RESULTS for <b>'.$out.'</b><pre>'; print_r( $result ); echo '</pre>';
        }
        else {
            echo '<label style="color: red; font-weight: bold;">No match found for</label> <b>'.$out.'<b/><br/>';
        }

        sleep(2);
    }
    ?>
   </div>
</div>
<?php endif; ?>